# 如何使用Mybatis-plus处理枚举值

#### 介绍
##### 1. 用户管理中存储男女对应的key值100、101

##### 2. maven依赖配置
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
    <!--  当前版本的mybatis-plus不需要在配置文件中定义type-enums-package属性  -->
    <dependency>
        <groupId>com.baomidou</groupId>
        <artifactId>mybatis-plus-boot-starter</artifactId>
        <version>3.5.3</version>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
        <version>1.18.24</version>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>8.0.31</version>
    </dependency>
</dependencies>
```


##### 3. 定义枚举类GenderEnum

```java
// GenderEnum枚举类
@AllArgsConstructor
public enum GenderEnum implements IEnum {

    MAIL(100, "男"),
    FEMAIL(101, "女"),
    UNKNOWN(102, "未知");
    ;
    // 存入数据库的value值
    @EnumValue
    private Integer value;
    // 返回到前端的值
    @JsonValue
    private String desc;

    @Override
    public Integer getValue() {
        return value;
    }


    @Override
    public String toString() {
        return this.desc;
    }
}


```




##### 5.结果

##### 1）插入
![img.png](img/get.png)
![img.png](img/img.png)

##### 2) 查询
![img_1.png](img/img.png)
![img_5.png](img/post.png)