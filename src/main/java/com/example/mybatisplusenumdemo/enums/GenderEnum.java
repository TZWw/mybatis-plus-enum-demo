package com.example.mybatisplusenumdemo.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.IEnum;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

/**
 * @author： Aaron
 * @date： 2022-11-18 15:00
 */
@AllArgsConstructor
public enum GenderEnum implements IEnum {

    MAIL(100, "男"),
    FEMAIL(101, "女"),
    UNKNOWN(102, "未知");
    ;

    @EnumValue
    private Integer value;

    @JsonValue
    private String desc;

    @Override
    public Integer getValue() {
        return value;
    }


    @Override
    public String toString() {
        return this.desc;
    }
}
