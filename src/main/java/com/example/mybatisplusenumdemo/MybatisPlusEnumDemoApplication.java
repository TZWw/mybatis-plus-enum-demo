package com.example.mybatisplusenumdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybatisPlusEnumDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MybatisPlusEnumDemoApplication.class, args);
	}

}
